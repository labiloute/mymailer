#!/usr/bin/env python
#-*- coding: utf-8 -*-
# IMPORTS
# Mail
import smtplib
from email.mime.multipart import MIMEMultipart
#from email.mime.multipart import MIMEApplication
from email.mime.text import MIMEText
from email.mime.base import MIMEBase # For attachements
from email import encoders # To encode attachements
from email.message import EmailMessage
from email.utils import formataddr # To encode FROM: correctly
import sys
# To get cli arguments
import getopt
# To sleep
import time
# To get hostname
import socket
hostname=socket.gethostname()
# To send to Mattermost
#import pycurl
import requests
# To send emails
import smtplib
# To create logs directory
import os
# Date is used in logs
import datetime
# pip3 install mysql-connector-python
from include import mysql
#import yaml (pip3 install oyaml)
try:
	import oyaml as yaml
except Exception as e:
	print("Please install oyaml dependancy : \"pip3 install oyaml\"")
	sys.exit(0)
from include import CLogs
Logs=CLogs.CLogs()

####################################
# Do not modify past this line
####################################

# Read YAML Main config file
try:
	with open("config/config.yaml", 'r') as stream:
		data_loaded = yaml.safe_load(stream)
except Exception as e:
	Logs.Log('FATAL','Your config file doesn\'t exists. Please copy the sample provided in config folder.')
	sys.exit(1)
config=data_loaded


# Import mail
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os

####### FN
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# Help Message

helppage="""
Usage : python3 mymailer.py
--send or --mail : send email to dest. DEFAULT IS THAT THE SOFT RUNS DRYRUN
-q or --query= : run a specific query name (i.e "-q sample.yaml")
-f or --force : force running disabled queries
-n or --nolog : do not write output in logs
-s or --sleep : sleep a few seconds between each dest
-h or --help : print this page :)
"""

directory_path = config['queries']['path']
files = [f for f in os.listdir(directory_path) if os.path.isfile(os.path.join(directory_path, f)) and f.endswith(".yaml")]
Logs.Log("INFO","Found {nbqueries} query files".format(nbqueries=len(files)),hidedate=True)

# Create query file
#msource=mysql.MysqlDB(config['db'],Logs)
# Count how many mails sent or in error
countmailok=0
countmailerr=0
#

# Open main log file only once
if 'maillogs' in config:
    # Get config value
    maillogs=config['maillogs']
    # Split to get only parent directory
    directory='/'.join(maillogs.split('/')[:-1])
    # Create parent directory if not exists
    os.makedirs(directory, exist_ok=True)
    # Open file
    maillogs = open(maillogs, "a")  # append mode

# Init default settings values
dryrun=True
selquery=False
force=False
log=True
clilog=True
clisleep=-1

# Try to grab arguments from user. "send" and "mail" are the same, they launch mail sending
try:
    # Get option. Short Argument are -f or -q . Q need to be followed by value, that is why it is followed by a :
    # Same way, long arguments are follow by a = if they require a value.
    opts, args = getopt.getopt(sys.argv[1:],"hfns:q:",["send","mail","force","nolog","sleep=","help","query="])
    #print("Opts : {}".format(opts))
    #print("Args : {} ".format(args))
    # Parse all Args
    for arg in args:
        if arg in ('send','mail'):
            dryrun=False
    # Parse all Options
    for name, value in opts:
        #print("Name:{} Value:{}".format(name,value))
        # Evaluate logging
        if name in ("--nolog","-n"):
            clilog=False
        # Evaluate dryrun
        if name in ("--mail","--send"):
            dryrun=False
        # Evaluate query
        if name=="-q" or name=="--query":
            selquery=value
        # Evaluate sleeping time
        if name in ("-s","--sleep"):
            clisleep=int(value)
        # Evaluate forcing
        if name in ("-f", "--force"):
            force=True
        # Display help page
        if name in ("-h", "--help"):
            print(helppage)
            sys.exit(0)
# Raise an error on wrong parameters given
except getopt.GetoptError as e:
    print(helppage)
    print(e)
    sys.exit(1)

# Display messages
if dryrun:
    Logs.Log("WARNING","Running dryrun. Please add \"mail\" or \"send\" as argument to force sending emails.")
if selquery:
    Logs.Log("WARNING","Running a single query file named : \"{}\"".format(selquery))
if not clilog:
    Logs.Log("WARNING","Logging totally disabled. Nothing will be append into log files, except this tty output.")

# Count how many queries where finaly ran
querycount=0

######################
# MAIN LOOP
######################
for queryfile in files:
    # Reset log to clilog value, because it can be individually disabled in QUERY OR IN TEMPLATE, OR WITH CLI ARG
    log=clilog
    sleep=clisleep

    # Filter is argument given
    if selquery and selquery!=queryfile:
        continue

    Logs.Log("DEBUG","#############################################")
    Logs.Log("DEBUG","####### {}".format(queryfile))
    Logs.Log("DEBUG","#############################################")

    # 1 - Open query
    with open("{directory_path}/{queryfile}".format(directory_path=directory_path,queryfile=queryfile), 'r') as stream:
        source = yaml.safe_load(stream)

    # 2 - Jump to next query if enabling is not explicit or if forcing is not enabled
    if 'enabled' in source and not source['enabled'] and not force:
        Logs.Log("DEBUG","Query {} not enabled, ignoring file.".format(queryfile))
        continue
    elif 'enabled' in source and not source['enabled'] and force:
        Logs.Log("WARNING","Forcing execution of this query, as requested")
    elif 'enabled' not in source:
        Logs.Log("WARNING","Ignoring a query without \"enabled\" parameter.")
        continue

    # 3 - Connect to dabatase (if provided)
    msource=mysql.MysqlDB(source['db'],Logs)

    # Check if logging is enabled or disabled and eventually disabled it (except if log disabled by --nolog arg)
    if 'log' in source and clilog:
        log=source['log']

    # Check if sleep time is provided by config file (overwritten by cli eventually)
    if 'sleep' in source and clisleep==-1:
        sleep=source['sleep']

    # Extract dest from queries. field with mail address MUST be named "email"
    dests=msource.query(source['query'])

    # If there is no results or if dest list is empty
    if msource.rowcount==0:
        Logs.Log("WARNING","Query returned {} results. Nothing will be sent.".format(msource.rowcount))

    # If dest count if null, we stop here
    if not dests:
        Logs.Log("ERROR","Something went wrong (or empty result) during query. Aborting.")
        continue

    # Eventually warning if there is a large number of dest
    if 'destslenwarning' in source and len(dests) > source['destslenwarning']:
        Logs.Log("WARNING","Warning, there is {} dest in this query. This is huge !.".format(len(dests)))

    # Eventually stop execution if there is more dests than in config
    if 'destslenexit' in source and len(dests) > source['destslenexit']:
        Logs.Log("FATAL","Error. The amount of dest ({}) is higher than setting 'destslenexit' which is set to {}. We stop !.".format(len(dests),source['destslenexit']))
        continue

    # Loop thru all dests
    for dest in dests:

	# Display dest
        Logs.Log("DEBUG","Working on dest : {}".format(dest['email']))

        # Loop thru all templates setup in config
        for template in source['templates']:

            # If template do not exists, pass with warning
            if not os.path.isfile("mails/{}.yaml".format(template)):
                Logs.Log("WARNING","Unable to find template mails/{}".format(template))
                continue

            # Open template
            with open("mails/{}.yaml".format(template), 'r') as stream:

                # Reset log to clilog value, because it can be individually disabled in QUERY OR IN TEMPLATE, or in CLI ARG
                log=clilog

                # Load file
                ctemplate = yaml.safe_load(stream)

                # Check if logging is enabled or disabled and eventually disabled it (except if log disabled by --nolog arg)
                if 'log' in ctemplate and clilog:
                    log=ctemplate['log']

                # Check if logging is enabled or disabled and eventually disabled it (except if log disabled by --nolog arg)
                if 'log' in source and clilog:
                    log=source['log']

                # Create a message multipart (plain & html)
                msgRoot = MIMEMultipart()

                # Set sender
                if 'sendername' in source and 'sender' in source:
                    #msgRoot['From']="{} <{}>".format(source['sendername'],source['sender'])
                    msgRoot['From']=formataddr((source['sendername'],source['sender']))
                elif 'sender' in source:
                    msgRoot['From'] = source['sender']
                else:
                    Logs.Log("ERROR","Unable to send an email from empty sender. Please add a sender to your query file.")
                    continue

                # Set recipient
                msgRoot['To'] = dest['email']

                # Construct content and subject
                if 'subject' in ctemplate: subject=ctemplate['subject']
                else: subject=''

                # Extract html content
                if 'contenttext' in ctemplate:contenttext=ctemplate['contenttext']
                else: contenttext=''

                # Extract plain text content
                if 'contenthtml' in ctemplate:contenthtml=ctemplate['contenthtml']
                else: contenthtml=''

                # Replace fields
                for fields in dest.keys():
                    # Replace field if provided by query
                    if dest[fields]:
                        # In subject
                        subject = subject.replace("["+fields+"]",str(dest[fields]))
                        # In HTML content
                        contenthtml=contenthtml.replace("["+fields+"]",str(dest[fields]))
                        # In text content
                        contenttext=contenttext.replace("["+fields+"]",str(dest[fields]))
                    # Or delete tag if not provided by query
                    else:
                        # In subject
                        subject = subject.replace("["+fields+"]","")
                        # In HTML content
                        contenthtml=contenthtml.replace("["+fields+"]","")
                        # In text content
                        contenttext=contenttext.replace("["+fields+"]","")

                # Set up Root message (Multipart message is a tree with a Root, and 2 alternatives attached)
                msgRoot['Subject'] = subject

                # Create an alternative which will be linked to root message - Record the MIME types of both parts - text/plain and text/html.
                msgAlternative = MIMEMultipart('alternative')
                msgRoot.attach(msgAlternative)

                # Create plain text content
                part1 = MIMEText(contenttext, 'plain')
                part1.add_header('Content-Type','text/plain')

                # Create html content
                part2 = MIMEText(contenthtml, 'html')

                # Attach plain and html parts into message container.
                # According to RFC 2046, the last part of a multipart message, in this case
                # the HTML message, is best and preferred.

                msgAlternative.attach(part1)
                #msgRoot.attach(part1)
                msgAlternative.attach(part2)

                # Eventually Include attachements
                if 'attachments' in ctemplate and isinstance(ctemplate['attachments'], list) and len(ctemplate['attachments'])>0:
                    for attachment in ctemplate['attachments']:
                        Logs.Log("DEBUG","Joining attachment : {}".format(attachment))
                        part = MIMEBase('application', "octet-stream")
                        part.set_payload(open("attachments/{}".format(attachment), "rb").read())
                        encoders.encode_base64(part)
                        part.add_header('Content-Disposition', 'attachment; filename="{}"'.format(attachment))
                        msgRoot.attach(part)


                # Send the message via local SMTP server.
                s = smtplib.SMTP(config['smtp']['host'])

                # Only send if first argument is "mail"
                if not dryrun:
                    # Refresh sending date time
                    dt=datetime.datetime.now()
                    dt=dt.strftime('%Y-%m-%d %H:%M:%S')

                    # Display sending message
                    Logs.Log("DEBUG","Sending email \"{}\" to {} !".format(template,dest['email']))

                    # SEND
                    sendingerrors=s.sendmail(source['sender'], dest['email'], msgRoot.as_string())

                    # Count and log errors or success (if not requested to ignore)
                    if log:
                        if len(sendingerrors)==0:
                            # Count for compact logs
                            countmailok+=1
                            # Write detailed logs
                            if log and not dryrun:
                                maillogs.write("{};{};{};OK\n".format(dt,template,dest['email']))
                        else:
                            # Count for compact logs
                            countmailerr+=len(sendingerrors)
                            # Write detailed logs
                            if log and not dryrun:
                                maillogs.write("{};{};{};{} ERRORS\n".format(dt,template,dest['email'],len(sendingerrors)))
                    else:
                        Logs.Log("WARNING","Logging disabled. Nothing will be written in maillogs")

                    # Increment how many query where ran
                    querycount+=1

                # Or only for debug purpose, print mail content
                else:
                    Logs.Log("DEBUG","DRYRUN : this email could have been sent to {}".format(dest['email']))
                    #print("Mode dry run, affichage du mail :")
                    #print("Version TEXTE du mail")
                    #print("Sujet : {}".format(subject))
                    #print(contenttext)
                    #print("Version HTML du mail")
                    #print("Sujet : {}".format(subject))
                    #print(contenthtml)

                # Eventually sleep between each dest
                if sleep:
                    Logs.Log("DEBUG","Sleeping a bit between each dest ({} seconds) !".format(sleep))
                    time.sleep(sleep)

            # Close connexion with mail server
            if s:
                s.quit()

    # Write to log file
    if log and 'compactlogs' in config and not dryrun:
        dt=datetime.datetime.now()
        dt=dt.strftime('%Y-%m-%d %H:%M:%S')
        # Get value
        compactlogs=config['compactlogs']
        # Split to get only parent directory
        directory='/'.join(compactlogs.split('/')[:-1])
        #print(directory)
        # Create parent directory if not exists
        os.makedirs(directory, exist_ok=True)
        # Open file
        compactlogs = open(compactlogs, "a")  # append mode
        # Write
        if log:
            compactlogs.write("{};{};{}\n".format(dt,countmailok,countmailerr))
            compactlogs.close()
    else:
        Logs.Log("WARNING","Logging disabled. Nothing written in compact log file.")

# Notice if a single query where asked but no query where ran
if selquery and querycount==0 and not dryrun:
    Logs.Log("WARNING","Query {} wasn't run. Not found ?".format(selquery))

# Close detailed logs
if maillogs: maillogs.close()

# With love !
