#!/usr/bin/env python
#-*- coding: utf-8 -*-
# To use exit...
import sys
import datetime

class CLogs():
   def __init__(self):
      self.level=1
      # DEBUG for debug mode, WARNING for warnings, ERROR for non-fatals errors (non exiting), FATAL for errors leading to exit.
      self.facility=('NONE','DEBUG','INFO','WARNING','ERROR','FATAL')
      # First init, we have no config available (Logs is the first boject to be created), so we init with loglevel=1
      #self.UpdateFacility(1)
      # Color
      self.color={}
      self.color['HEADER'] = '\033[95m'
      self.color['OKBLUE'] = '\033[94m'
      self.color['INFO'] = '\033[94m'
      self.color['OKGREEN'] = '\033[92m'
      self.color['WARNING'] = '\033[93m'
      self.color['FATAL'] = '\033[91m'
      self.color['ENDC'] = '\033[0m'
      self.color['DEBUG'] = '\033[0m'
      self.color['NONE'] = '\033[0m'
      self.color['ERROR'] = '\033[1m'
      self.color['BOLD'] = '\033[1m'
      self.color['UNDERLINE'] = '\033[4m'
#
# Return true if an arguement is present
#

   def SetConfig(self,Config):
      self.Config=Config
      level=int(self.Config.GetValue('SectionGlobals','debuglevel'))
      self.UpdateFacility(self.level)

   def Log(self,facility,msg,hidedate=False):
      now = datetime.datetime.now()
      if not hidedate:
          logdatetime="["+now.strftime('%Y-%m-%d %H:%M:%S')+"]"
      else:
          logdatetime=""
      # Get key of the message facility
      key=self.facility.index(facility)
      # If key is superior or equal to config, print
      if key>=self.level:
         print(self.color[facility] + "[{facility}]{logdatetime} {msg}".format(facility=facility,logdatetime=logdatetime,msg=msg) + self.color['ENDC'])

#
# Update facility
#
   def UpdateFacility(self,level):
      level=int(level)
      if self.level != level and level>0 and level<=4:
         print(self.color['UNDERLINE'] + "Updating debug facility from {} to {} and above.".format(self.facility[self.level],self.facility[level]) + self.color['ENDC'])
         self.level = level
