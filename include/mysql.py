#!/usr/bin/env python
#-*- coding: utf-8 -*-
import sys
#from python_mysql_dbconfig import read_db_config
# pip3 install mysql-connector

# Source : http://www.mysqltutorial.org/python-mysql-query/
try:
	import mysql.connector
except Exception as e:
	self.Logs.Log("FATAL","Dependancy : please install python mysql connector : \"pip3 install mysql-connector\"")
	self.Logs.Log("DEBUG","Error was {}".format(e))
	sys.exit(1)

class MysqlDB():
#
	def __init__(self,CONFIG,Logs):
		# Init
		self.CONFIG=CONFIG
		self.Logs = Logs

		#print(CONFIG)
		try:
			self.conn = mysql.connector.MySQLConnection(**self.CONFIG)
		except Exception as e:
			self.Logs.Log("ERROR","Unable to connect to DB with options {}".format(self.CONFIG))
			self.Logs.Log("DEBUG","Error was {}".format(e))
#
	def getQueryModel(self,q,table):
		# Temporarily give a limit to query will speed up model grabbing
		if "LIMIT" not in q:
			q+=" LIMIT 1 "
		temp_table="CREATE TEMPORARY TABLE {table} \n {q};\nSHOW CREATE TABLE {table}".format(q=q,table=table)
		#temp_table="CREATE TEMPORARY TABLE xx_temp_table_xx \n {q};\nSHOW CREATE TABLE xx_temp_table_xx".format(q=q)
		#print(temp_table)
		model=self.query(temp_table,multi=True)
		sql=model[0][1]
		#print(sql.replace("CREATE TEMPORARY TABLE", "CREATE TABLE"))
		if sql:
			sql=sql.replace("CREATE TEMPORARY TABLE", "CREATE TABLE IF NOT EXISTS ")
		return sql
#
	def columns(self,table):
		columns=self.query("SELECT * FROM information_schema.columns WHERE table_name='" + table + "';")
		return columns
#
	def query(self,q, commit=False,multi=False):
		# Init
		self.rowcount=0
		table=()
		cursor = self.conn.cursor(dictionary=True)
		#self.conn = MySQLConnection(**self.CONFIG['db'])
		# Vilain hack pur executer des requetes multi : https://stackoverflow.com/questions/15288594/update-database-with-multiple-sql-statments
		if multi:
			for result in cursor.execute(q, multi=True):
				pass
		else:
			# Run query
			try:
				cursor.execute(q)
			except mysql.connector.Error as err:
				if err.errno == 1068:
					self.Logs.Log("WARNING","Mysql said : {e}".format(e=err.msg))
				elif err.errno == 1050:
					self.Logs.Log("WARNING","Mysql said : {e}".format(e=err.msg))
				elif err.errno == 144:
					self.Logs.Log("ERROR","Tables are marked as crashed unable to go further :  {e}".format(e=err.msg))
				else:
					self.Logs.Log("ERROR","Mysql said : Error {n} - {e}".format(n=err.errno,e=err.msg))
				return False
		#allrows=cursor.fetchall()
		if commit:
			try:
				self.conn.commit()
			except Error as err2:
				#if err.errno == 144:
				#self.Logs.Log("[ERROR]", "Unable to commit changes, rolling back.")
				self.Logs.Log("[ERROR]", "Unable to multi query, rolling back.")
				self.conn.rollback()
		#print(err)
		#print("Error Code:", err.errno)
		#print("SQLSTATE", err.sqlstate)
		#print("Message", err.msg)
		"""
		except Error as e:
			print("[WARNING] Warning during querying : {q}".format(q=q))
			print(e)
		"""
		# Retrieve rows, x by x
		results = cursor.fetchall()
		self.rowcount=cursor.rowcount
		return results
#

	"""
	def insert_sql(self,q):
		cursor = self.conn.cursor()
		cursor.execute(q)
		try:
			self.conn.commit()
		except:
			print("[ERROR] Problem inserting data. Rollback.")
			self.conn.rollback()
	"""
#
	def insert(self,d,table):
		# Init
		counta=0
		self.insertrowcount=0
		self.rowcount=0
		# Get columns names
		cols=self.columns(table)
		#print("Number of columns in table : "+len(cols[0]))
		# For each what row "i" of the table "d"
		for i in d:
			dup=""
			#q="REPLACE INTO {table} VALUES(".format(table=table)
			q="INSERT INTO {table} VALUES(".format(table=table)
			#print(i)
			countb=0
			# For each column "f" of the row "i"
			for f in i:
				#print("Number of columns in data : "+len(f))
				# Try to decode bytearray field (imported from mysql), or convert it to string
				try:
					#print(f)
					if f:
						f = f.decode()
					#print(f)
				except:
					f=str(f)
				# Convert None to NULL
				if not f:f='NULL'
				# Escaping string
				else: f=self.conn.converter.escape(f)
				# Add comma to field except if it's the first one
				#print (f)
				# Add a comma to both INSERT and UPDATE list
				if countb>0:
					q+=","
					dup+=","
				# Add ' to protect field, except for NULL values
				if f == 'NULL':
					q+=str(f)
					#if (f!="id" and f!='ID'):
					dup+=""+str(cols[countb][3])+" = "+str(f)+" \n"
				else:
					q+="'"+str(f)+"'"
					#if (f!="id" and f!='ID'):
					dup+=""+str(cols[countb][3])+" = '"+str(f)+"' \n"
				# +1
				countb+=1
			# Close query
			q+=")"
			q+=" ON DUPLICATE KEY UPDATE "+dup
			#print(q)
			counta+=1
			self.query(q,commit=True)
			self.insertrowcount+=self.rowcount
			#if counta==100:
			#	break


#
	def iter_row(self,cursor, size=10):
		while True:
			rows = cursor.fetchmany(size)
			if not rows:
				break
			for row in rows:
				yield row

#

	def close(self):
		self.conn.close()

#
	def addStartDate(self,q,startdatepattern,startdate):
		if startdatepattern in q:
			self.Logs.Log("DEBUG","Query will be filtered by results starting from {startdate}".format(startdate=startdate))
			#self.Logs.Log("DEBUG","Replacing pattern to start date in query.")
			# Replace pattern by a start date in configuration
			q=q.replace(startdatepattern, str(startdate))
		else:
			self.Logs.Log("WARNING","Query does not contain a date filter. It could really impact performance.")
		#print(q)
		return q


'''
	def connect():
		""" Connect to MySQL database """
		try:
			conn = mysql.connector.connect(host='localhost',
										   database='python_mysql',
										   user='root',
										   password='secret')
			if conn.is_connected():
				print('Connected to MySQL database')
	 
		except Error as e:
			print(e)
	 
		finally:
			conn.close()
'''
